這個目錄有兩個合約跟兩個腳本
兩個腳本包括depl.sh以及deplMain.sh
他們之間的差異只在於depl.sh適用在regression mode
而deplMain.sh適用在mainnet
---

兩個合約分別為aid.c以及recording.c

aid.c的功能很簡單
他接收兩個參數分別是一個uuid及一個publickey
之後將他們以成對的方式寫入ourchain並紀錄到$(HOME)/.bitcoin/aid.txt
紀錄的格式為：
uuid:publickey
相異的aid以斷行隔開
---

recording.c的功能有兩種
第一種：
    單純紀錄AC指紋用的
    一個是紀錄評分紀錄用的

    紀錄指紋的功能單純將不同的指紋寫入ourchain並紀錄到$(HOME)/.bitcoin/ac.txt
    相異的aid以斷行隔開
    example: bitcoin-cli callcontract 'contractAddress' register 'fingerprint'

第二種：
    紀錄評分的功能則是將一筆評分寫入到$(HOME)/.bitcoin/rate.txt
    紀錄的格式每一筆為：
        被評分的AC指紋:評分人使用的AID:給出的分數:時間戳:簽章
    相異的評分以斷行隔開

    使用範例: bitcoin-cli callcontract 'contractAddress' rate 'fingerprint' 'aid' 'score' 'timestamp' 'signature'