# 一個基本的demo流程如下
---

## 先安裝必要的python套件

    sudo apt install python3-pip
    pip3 install ecdsa flask

## 資料夾結構如下

    總共有四個資料夾contract, aidwallet, issuer, verifier
### contract資料夾
contract資料夾內包括兩個腳本:depl.sh, deplMain.sh 以及兩個合約: aid.c, recording.c,
AID合約會幫忙紀錄每個新註冊的AID,
recording.c也就是AC合約, 會負責紀錄兩種不同內容, 一種是AC的評分, 另一種是新AC在發布時需要紀錄的指紋,
而depl.sh及deplMain.sh分別是兩個幫忙部屬合約的腳本,
depl.sh用在regressionMode的ourchain上,
而在Mainnet上我們要執行的是deplMain.sh
詳細的aidwallet可以參照[contract的使用請參考這個markdown](contract/readme.md)
    
### aidwallet資料夾
裡面包含aidwallet.py, walletconfig.json, document.txt,
其中aidwallet.py是錢包程式的主體,
walletconfig.json是錢包程式的相關設定檔,
document.txt則是一個用來demo向lab408的issue網站申請AC所用到的文件,
詳細的aidwallet可以參照[wallet的使用請參考這個markdown](adiwallet/readme.md),
或是執行python3 aidwallet.py help查看指令,

### issuer資料夾
因為在派發AC的過程中也需要用到aid錢包,
因此我們在issuer資料夾內也放了一套錢包主程式跟設定檔:aidwallet.py, walletconfig.json,
至於issuer.py以及templates資料夾下的內容就是lab408的AC派發網站之主體,
詳細的lab408 AC派發網站可以參考[issuer網站的API請參考這個markdown](issuer/readme.md),
至於ans.txt檔案則是一個用來demo lab408的AC派發往站在審核AC派發申請時會用到的文件

### verifier資料夾
包括兩個python程式 forum.py, verificationAC.py以及一些在templates資料夾底下的html檔,
forum.py是論壇的主體, 這個論壇能接受aid以及AC登錄, 並且提供登錄者發布文章,
而verificationAC就是一套幫助forum驗證AC的模組,主要幫忙確認AC註冊紀錄跟驗證AC上的簽章,
詳細的API可以參考[forum網站的API請參考這個markdown](verifier/readme.md)
    

## 將AID及AC合約部屬
請確保部屬合約前有足夠的資金

    bitcoin-cli generate 101 #先挖到足夠的資金
    再進到 contract 資料夾
    # ./depl.sh   Note. regression mode執行這行
    ./deplMain.sh  Note. mainnet執行這行 
    
    第一個部屬的合約是AID合約 它會幫忙紀錄每個新註冊的AID
    第二個部屬的是AC合約 它會負責紀錄兩種不同內容 一種是AC的評分 另一種是新AC在發布時需要紀錄的指紋
    請開啟aidwallet及issuer資料夾中的walletconfig.json
    將第一個合約的地址替換AIDcontract的值
    將第二個合約的地址替換ACcontract的值


## 執行forum網站跟lab408的發布AC網站
這邊 forum 在demo中擔任 verifier 的角色
而lab408的AC發布網站就是擔任 issuer 的角色
lab408的AC發布網站會根據使用者提供的申請文件來決定是否派發AC
派發條件是申請文件必須要跟issuer資料夾底下的ans.txt內容一樣
我們透過這種通關密碼的方式來假設該用戶是lab408成員
forum則是一個論壇提供各種使用者使用不同的AID AC進行登錄
爾後使用者使用AC登錄時 網站會將該AC的內容主題顯示在每篇他發的文章上以進行demo
forum預設執行在 5001 port
lab408的發布AC網站預設執行在 5002 port**

    進到verifier資料夾
    python3 forum.py  #啟動forum
    進到issuer資料夾
    python3 issuer.py  #啟動lab408的AC發布網站

## 替issuer創造一個AID
在issuer發布任何AC前，他都需要先有一個AID來進行擔保，
為此,我們先替lab408的AC發布網站生成AID以利其後續透過該AID來發布AC,
生成的方式是透過進入到issuer資料夾底下的錢包app aidwallet.py,
NOTE： lab408的AC發布網站當前會用錢包中的第一筆AID來發起AC

    進入issuer資料夾
    python3 aidwallet.py createAID
    python3 aidwallet.py listAID

## 扮演holder完成整個用AID或AC登錄forum的demo
一旦前面準備就緒
我們就能進到aidwallet資料夾,
開始扮演一個holder的角色,
我們先生成一個AID以便後續拿他申請AC,
之後透過剛剛申請的AID去向lab408的發布AC網站申請帶有lab408內容的AC,
之後就能將拿到的AC去forum進行登錄的動作,
最後我們介紹一下AID登錄,
AID登錄的手續就很簡單,只需要在本地生成後,就能去接收AID登錄的網站進行登錄,
例如下面指令區塊的最後一行就是拿前面為了申請lab408AC所產生的AID去forum進行登錄

    進入aidwallet資料夾
    python3 aidwallet.py createAID
    python3 aidwallet.py listAID
    python3 aidwallet.py requestAC 0 document.txt http://127.0.0.1:5002 
    python3 aidwallet.py listAC
    python3 aidwallet loginAC 0 http://127.0.0.1:5002/submitAID 
    # AID登錄demo
    python3 aidwallet.py loginAID 0 http://127.0.0.1:5002/submitAC
