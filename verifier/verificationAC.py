from ecdsa import SigningKey, SECP256k1, VerifyingKey
import json
import requests
import hashlib
import os

# Verify an AC by verify its signature and check the registeration record of it.
def verifyAC(AC):
    ACjson = json.loads(AC)
    if(('holderAID' not in ACjson) or ('issuerAID' not in ACjson) or ('timestamp' not in ACjson) or ('subject' not in ACjson) or ('signature' not in ACjson)):
        return False, "wrong AC format"
    ACjson2 = json.loads(AC)
    sig = ACjson2['signature']
    sigbyte = bytes.fromhex(sig)
    del ACjson2['signature']

    AIDs = []
    try:
        with open(os.getenv("HOME")+'/.bitcoin/aid.txt') as file:
            AIDs = file.readlines()
            AIDs = [line.rstrip() for line in AIDs]
    except:
        return False, "smt wrong with the contract"
    issueUid = ''
    issuePk = ''
    for aid in AIDs:
        aidok = str(aid).split(":")
        print('l:', aidok[0], ACjson['issuerAID'])
        if(aidok[0] == ACjson['issuerAID']):
            issueUid = aidok[0]
            issuePk = aidok[1]
    
    if(issueUid==''):
        print("no aid found")
        return False, "no aid found"
    vk = VerifyingKey.from_string(bytes.fromhex(issuePk), curve=SECP256k1)
    s = hashlib.sha256()
    j = json.dumps(ACjson2).encode('utf-8')
    s.update(j)
    if not vk.verify(sigbyte, bytes.fromhex(s.hexdigest())):
        return False, "signature verification error"

    fingerprints = []
    try:
        with open(os.getenv("HOME") + '/.bitcoin/ac.txt') as file:
            fingerprints = file.readlines()
            fingerprints = [line.rstrip() for line in fingerprints]
    except:
        return False, "smt wrong with the contract"


    s = hashlib.sha256()
    j = AC.encode('utf-8')
    s.update(j)
    fingerPrint = s.hexdigest()
    findfinger = False
    for finger in fingerprints:
        if(finger == fingerPrint):
            findfinger = True
    if(not findfinger):
        return False, "AC registeration not found"
    return True, ""