from flask import Flask, request, Response, jsonify, render_template, make_response, redirect, url_for
from ecdsa import SigningKey, SECP256k1, VerifyingKey
import json
import requests
import random
import string
import hashlib
import filecmp
import os
import subprocess


if not os.path.exists('whoWantAuthenticate.json'):
    with open('whoWantAuthenticate.json', 'w') as f:
        f.write('[]')

app = Flask(__name__)

# Return homepage
@app.route('/') 
def index():
    return render_template('index.html')

# An API for user submit AID and document for requesting an AC
@app.route('/', methods=['POST'])
def upload_file():
    fine = False
    reqAID = request.form['AID']
    uploaded_file = request.files['file']
    if uploaded_file.filename != '':
        uploaded_file.save("doc.txt")
        #qualification review, issuer can custmize their qualification strategy,
        #line 35-36 is our qualification strategy,
        #we ask user to submit a file with the same content as the ans.txt in this directory
        if(filecmp.cmp("doc.txt", "ans.txt")):
            fine = True
        os.remove("doc.txt")
    if(fine):
        AIDs = []
        try:
            with open(os.getenv("HOME")+'/.bitcoin/aid.txt') as file:
                AIDs = file.readlines()
                AIDs = [line.rstrip() for line in AIDs]
        except:
            return "ourchain not working"
        for aid in AIDs:
            aidok = str(aid).split(":")
            if(aidok[0] == reqAID):
                message = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
                temp = 0
                data = []
                with open('whoWantAuthenticate.json', 'r', newline='') as jsonfile:
                    data = json.load(jsonfile)
                    for i, val in enumerate(data):
                        if(reqAID==val['aid']):
                            data[i]['message'] = message
                            temp = 1
                if(temp == 0):
                    data.append({ \
                        'aid': reqAID, \
                        'pk': aidok[1], \
                        'message': message \
                    })
                with open('whoWantAuthenticate.json', 'w', newline='') as jsonfile:
                    json.dump(data, jsonfile)
                respData = {"message": message, "authAPI": "authenticateAID"}
                return jsonify(respData)

        return "Invalid AID"
    else: return "Wrong document!"

# Before issueing an AC to an AID, we need the AID authentication process
# to make sure the requester have the control of the AID he/she provide
# here is the API for authenticate an AID
@app.route('/authenticateAID', methods=['GET', 'POST'])
def AIDauthentication():
    # We return an authentication page for user 
    # when the user use method 'GET' to this API
    if request.method == "GET":
        return render_template('aidauthenticate.html')
    # We authenticate an AID by the AID and signature provide by the user 
    # when the user use method 'POST' to this API    
    if request.method == "POST":
        aid = request.values.get('AID', None)
        sig = request.values.get('sig', None)
        data = []
        vk = None
        login = False
        with open('whoWantAuthenticate.json', newline='') as jsonfile:
            data = json.load(jsonfile)
        for i in data:
            if(aid == i['aid']):
                vk = VerifyingKey.from_string(bytes.fromhex(i['pk']), curve=SECP256k1)
                try:
                    login = vk.verify(bytes.fromhex(sig), bytes(i['message'], 'utf-8'))
                except:
                    return 'login falied , '+str(aid)
        if(vk == None):
            return "you have not request AC here before, please request AC by 'POST' the API '/' first"
        if(login):
            try:
                #Issue lab408 AC to aid by the first AID stored in wallet
                runwallet = subprocess.check_output("python3 aidwallet.py issueAC " + aid + " lab408 0", shell=True)
            except:
                print("issue failed: something wrong with the wallet issueAC")
                return "issue failed"
            return runwallet[0:-1].decode("utf-8")
            
        print("login failed")
        return 'login falied , '+str(aid)

# running this app on port 5002
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5002)