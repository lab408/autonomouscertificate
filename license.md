# Benevolence license
1. If you are a natural person, you are free-licensed for personal usage.
2. Not a natural person, such as a legal person, group, or organization should follow [Apache license 2.0](https://www.apache.org/licenses/LICENSE-2.0), provided that ==benevolence(仁), [neutral dao for humanity](https://hackmd.io/QMdKVF_0R3WpBTopqkt-TQ?view#Neutral-Number), autonomy then sharing==, is default supported by a signed contract; otherwise, name of the violator will be broadcast.
    1. Autonomy: any person involved in this license is free to do anything legally, provided that no one can violate the autonomy of any one.
    2. Sharing: any product (not limited to fame, source, object, or profit) should be reasonably shared to all participants. 
3. If consensus of autonomy or sharing cannot be reached, please quit and leave peacefully, following contracts if exists. Strife clause is termed at least one week. Do no evil to any one involved.
    1. [How to share](/mIRxzwPVQn-eBadOxb4VSQ) needs to be included in this license including the access to [the list of license violators](https://hackmd.io/mIRxzwPVQn-eBadOxb4VSQ?view#Faked-Benevolence-Projects).
    2. No redistribution right, unless being explicitly specified in contract.
4. end of license.