#include <ourcontract.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct state{
	char uuid[300];
    char pk[1000];
} State;

int contract_main(int argc, char *argv[])
{
    State states[1000];
    int i,size;
    state_read(&size,sizeof(int));
    state_read(states,size*sizeof(State));
	char uuid[300];
	char pk[1000];
	strcpy(uuid,argv[1]);
	strcpy(pk,argv[2]);
    strcpy(states[size].uuid,uuid);
	strcpy(states[size].pk,pk);
    size+=1;
    state_write(&size,sizeof(int));
	state_write(states,size*sizeof(State));
	
	FILE *out;
	char homepath[20];
	char filepath[50];
	strcpy (homepath, getenv("HOME"));
	sprintf(filepath, "%s/.bitcoin/aid.txt", homepath);
	out=fopen(filepath, "w+");
	if(out != NULL) {
		for(i=0;i<size;i++) {
			fprintf(out, "%s:%s\n",states[i].uuid, states[i].pk);
		}
	}

    return 0;
}

