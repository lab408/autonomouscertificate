from flask import Flask, request, Response, jsonify, render_template, make_response, redirect, session
from ecdsa import SigningKey, SECP256k1, VerifyingKey
from verificationAC import verifyAC
from datetime import timedelta
import json
import requests
import random
import string
import hashlib
import os


if not os.path.exists('AIDsubmitters.json'):
    with open('AIDsubmitters.json', 'w') as f:
        f.write('[]')
if not os.path.exists('ACsubmitters.json'):
    with open('ACsubmitters.json', 'w') as f:
        f.write('[]')
if not os.path.exists('posts.json'):
    with open('posts.json', 'w') as f:
        f.write('[]')

app = Flask(__name__)
# session config
app.config['SECRET_KEY'] = os.urandom(24) 
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=31)



# return index.html if no session found else return indexalreadylogin.html
@app.route('/')
def homepage():
    if request.method == "GET":
        uid = session.get('userID')
        loginType = session.get('loginType')
        posts = []
        with open('posts.json', newline='') as jsonfile:
            posts = json.load(jsonfile)
        if uid == None:
            return render_template("index.html", posts = posts)
        else:
            return render_template("indexalreadylogin.html", posts = posts, loginID = uid, loginType = loginType)


# Post an article to the forum
@app.route('/post', methods=['GET', 'POST'])
def post():
    if request.method == "GET":
        uid = session.get('userID')
        loginType = session.get('loginType')
        if uid == None or loginType == None:
            return "please login first , <a href=\"login\">login</a> "
        else:
            return render_template("editpost.html")
    elif request.method == "POST":
        posts = []
        with open('posts.json', newline='') as jsonfile:
            posts = json.load(jsonfile)
            title = request.values.get('title')
            content = request.values.get('content')
            #uid = request.cookies.get('userID')
            #loginType = request.cookies.get('loginType')
            uid = session.get('userID')
            loginType = session.get('loginType')
            acContent = session.get('contentAC')
            if(acContent != None):
                if content !='' and title != '' and uid != None and loginType != None:
                    posts.append({ \
                        'title': title, \
                        'content': content, \
                        'author': uid, \
                        'idtype': loginType, \
                        'acsub': acContent \
                    })
                    with open('posts.json', 'w', newline='') as jsonfile:
                        json.dump(posts, jsonfile)
                    return redirect("/", code=302)
            else:
                if content !='' and title != '' and uid != None and loginType != None:
                    posts.append({ \
                        'title': title, \
                        'content': content, \
                        'author': uid, \
                        'idtype': loginType \
                    })
                    with open('posts.json', 'w', newline='') as jsonfile:
                        json.dump(posts, jsonfile)
                    return redirect("/", code=302)
            return "post failed , <a href='/'>return home</a>"
        

# return "login.html" for user to make login
@app.route('/login')
def loginpage():
    if request.method == "GET":
        return render_template("login.html")


# delete the session for the logout user
@app.route('/logout')
def logout():
    posts = []
    with open('posts.json', newline='') as jsonfile:
        posts = json.load(jsonfile)
    resp = make_response(render_template("index.html", posts = posts))
    session['userID'] = False
    session['loginType'] = False
    session['contentAC'] = False
    return resp


# API for accept the AID submitted from user
@app.route('/submitAID', methods=['GET', 'POST'])
def submitAID():
    uid = request.form['uid']
    print('uid = ', uid)
    AIDs = []
    try:
        with open(os.getenv("HOME")+'/.bitcoin/aid.txt') as file:
            AIDs = file.readlines()
            AIDs = [line.rstrip() for line in AIDs]
    except:
        return "something wrong with the contract"
    for aid in AIDs:
        aidok = str(aid).split(":")
        if(aidok[0] == uid):
            print(aidok[0], ' try to login',)
            # generate a random message to user sign for making AID authentication
            message = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
            temp = 0
            data = []
            with open('AIDsubmitters.json', newline='') as jsonfile:
                data = json.load(jsonfile)
                for i, val in enumerate(data):
                    if(uid==val['uid']):
                        data[i]['message'] = message
                        temp = 1
            if(temp == 0):
                data.append({ \
                    'uid': uid, \
                    'pk': aidok[1], \
                    'message': message \
                })
            with open('AIDsubmitters.json', 'w', newline='') as jsonfile:
                json.dump(data, jsonfile)
            respData = {"message": message, "authAPI": "authenticateAID"}
            return jsonify(respData)
    return "Invalid AID"


# Before accepting an AID, we need the AID authentication process
# to make sure the submitter actually have the control of the AID he/she provide
# here is the API for authenticate an AID
@app.route('/authenticateAID', methods=['GET', 'POST'])
def authenticateAID():
    uid = request.values.get('uid', None)
    sig = request.values.get('sig', None)
    data = []
    login = False
    with open('AIDsubmitters.json', newline='') as jsonfile:
        data = json.load(jsonfile)
    for i in data:
        if(uid == i['uid']):
            vk = VerifyingKey.from_string(bytes.fromhex(i['pk']), curve=SECP256k1)
            try:
                login = vk.verify(bytes.fromhex(sig), bytes(i['message'], 'utf-8'))
            except:
                return 'login falied , '+str(uid)
    if(login):
        resp = redirect("/", code=302)
        session['userID'] = uid
        session['loginType'] = "AID"
        session['contentAC'] = False
        session.permanent = True
        return resp
    return 'login falied , '+str(uid)


# API for accept the AC submitted from user
@app.route('/submitAC', methods=['GET', 'POST'])
def submitAC():
    AC = request.form['AC']
    res, theErr = verifyAC(AC)
    ACjson = json.loads(AC)
    if(not res):
        return theErr
    if(('holderAID' not in ACjson) or ('subject' not in ACjson)):
        return "wrong AC format"
    AIDs = []
    try:
        with open(os.getenv("HOME")+'/.bitcoin/aid.txt') as file:
            AIDs = file.readlines()
            AIDs = [line.rstrip() for line in AIDs]
    except:
        return "something wrong with the contract"
    # the uid of the holder of this AC
    loginAID = ''
    for aid in AIDs:
        aidok = str(aid).split(":")
        if(aidok[0] == ACjson['holderAID']):
            loginAID = aidok[0]
            print('AC holder : ',aidok[1],", try to login")
            # generate a random message to user sign for making AID authentication
            message = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
            temp = 0
            data = []
            with open('ACsubmitters.json', newline='') as jsonfile:
                data = json.load(jsonfile)
                for i, val in enumerate(data):
                    if(data[i]['aid']==ACjson['holderAID']):
                        data[i]['message'] = message
                        data[i]['acsub'] = ACjson['subject']
                        temp = 1
            if(temp == 0):
                data.append({ \
                    'aid': loginAID, \
                    'pk': aidok[1], \
                    'message': message, \
                    'sub': ACjson['subject']
                })
            with open('ACsubmitters.json', 'w', newline='') as jsonfile:
                json.dump(data, jsonfile)
            respData = {"message": message, "authAPI": "authenticateAC"}
            return jsonify(respData)
    return "Invalid uuid"


# Before accepting an AC, we need the AC authentication process
# to make sure the submitter actually have the control of the  AC he/she provide
# here is the API for authenticate an AC
# An AC authentication is actually doing aid authentication to the holder AID on that AC
@app.route('/authenticateAC', methods=['GET', 'POST'])
def authenticateAC():
    theaid = request.values.get('aid', None)
    if theaid == None:
        theaid = request.values.get('uid', None)
    sig = request.values.get('sig', None)
    #print(theaid)
    #print(sig)
    data = []
    contentAC = ''
    login = False
    with open('ACsubmitters.json', newline='') as jsonfile:
        data = json.load(jsonfile)
    for i in data:
        if(theaid == i['aid']):
            contentAC = i['sub']
            vk = VerifyingKey.from_string(bytes.fromhex(i['pk']), curve=SECP256k1)
            try:
                login = vk.verify(bytes.fromhex(sig), bytes(str(i['message']), 'utf-8'))
            except:
                return 'login falied , '+str(theaid)
    if(login):
        resp = redirect("/", code=302)
        session['userID'] = theaid
        session['loginType'] = "AC"
        session['contentAC'] = contentAC
        session.permanent = True
        return resp

    return 'login falied , '+str(theaid)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5001)