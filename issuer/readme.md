**Home Page**
----
  返回首頁.

* **URL**
  /
  
* **Method:**
  `GET`
  
---
**Request an AC**
----
  根據使用者提供的AID及申請文件來進行資格審查,決定該用戶是否具備被派發AC的資格.

* **URL**
  /
  
* **Method:**
  `POST`

* **Data Params**
`AID=[uuidv4]`
`file=[document]  #用來資格審查的文件` 

  
---
**Page for AID authentication**
---
  返回一個頁面讓使用者填入AID認證所需要的AID及簽章 

* **URL**
  /authenticateAID
  
* **Method:**
  `GET`

  
---
**AID authentication**
----
  根據使用者提供的AID及簽章來對該其進行AID的身份認證.

* **URL**
  /authenticateAID
  
* **Method:**
  `POST`

* **Data Params**
`AID=[uuidv4]`
`sig=[string]  #用來幫助AID身份認證的簽章` 

---