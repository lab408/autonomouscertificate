echo "deploying in regression mode"
echo "------------------------------"
echo "deploying AIDcontract : "
bitcoin-cli --regtest deploycontract $(pwd)/aid.c
bitcoin-cli --regtest generate 1
echo "------------------------------"
echo "deploying ACcontract : "
bitcoin-cli --regtest deploycontract $(pwd)/recording.c
bitcoin-cli --regtest generate 1