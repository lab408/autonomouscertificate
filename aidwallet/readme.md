walletconfig.json是config檔用來設定一些aidwallet.py會使用到的路徑或合約地址
"AIDcontract":紀錄AID的ourcontract地址
"ACcontract":紀錄AC的ourcontract地址
"AIDdb":保存所有使用者AID的本地資料庫
"ACdb":保存所有使用者AC的本地資料庫

## AID related
createAID 新增一個AID
透過生成一筆uuidv4跟公司鑰對來建立一AID
並將此AID存在myAID.json的本地資料庫

delAID "index" 刪除index所對應的AID
index是依照AID在本地資料庫的順序所提供的一個編號
使用者可先透過listAID來找到他們所要刪除的AID之對應編號

listAID 從本地資料庫列出使用者的所有AID

loginAID "index" "url" 透過index編號所指定的AID對url所指到的登錄API去對該網站進行登錄
index是依照AID在本地資料庫的順序所提供的一個編號
url是提供AID登錄的網站之API位址
如果沒有提供url會預設請求http://127.0.0.1:5001/submitAID

sign "index" "message" 透過index編號所指定的AID對message內容進行簽章並返回該AID之uuid以及簽章結果 

## AC related
delAC "index" 刪除index所對應的AC
index是依照AC在本地資料庫的順序所提供的一個編號
使用者可先透過listAC來找到他們所要刪除的AID之對應編號

issueAC "holderAID" "subject" "AIDindex" 透過AIDindex所指的AID來發布具有subject內容的AC給holderAID

listAC 從本地資料庫列出使用者的所有AID

loginAC "index" "url" 透過index編號所指定的AC對url所指到的登錄API去對該網站進行登錄
index是依照AC在本地資料庫的順序所提供的一個編號
url是提供AC登錄的網站之API位址
如果沒有提供url會預設請求http://127.0.0.1:5001/submitAC

rateAC "ACtobeRated" "score" "raterAIDindex" 透過raterAIDindex所指的AID來對ACtobeRated這個AC給出分數score的評價
score只能是1~5的整數

requestAC "indexAID" "docName" "requestAPI" 將indexAID所指到的AID跟docName所代表的文件一同附上給requestURL網站來請求該網站發布AC給使用者
如果沒有提供url會預設請求http://127.0.0.1:5002/
成功後會將申請到的AC存放在本地的AC資料庫
可透過listAC查看

storeAC "AC" 將接收到的AC存入本地資料庫
