from ecdsa import SigningKey, SECP256k1
import uuid
import requests
import json
import sys
import os
import hashlib
import time
import webbrowser
#config

f = open('walletconfig.json',)
config = json.load(f)
dbAID = config['AIDdb']
dbAC = config['ACdb']
AIDcontract = config['AIDcontract']
ACcontract = config['ACcontract']

if not os.path.exists(dbAID):
    with open(dbAID, 'w') as f:
        f.write('[]')
if not os.path.exists(dbAC):
    with open(dbAC, 'w') as f:
        f.write('[]')


# Create an AID by generate its keypair and uuidv4, then register to ourchain by calling the contract.
def createAID():
    uid = uuid.uuid4()
    sk = SigningKey.generate(curve=SECP256k1)
    vk = sk.verifying_key
    data = []
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
    with open(dbAID, 'w', newline='') as jsonfile:
        data.append({ \
            "uid": str(uid), \
            "sk": sk.to_string().hex(), \
            "pk": vk.to_string().hex() \
        })
        json.dump(data, jsonfile)
    
    ''' test if the pubkey and seckey work
    my_data = {"uid":str(uid), "pubkey":vk.to_string().hex()}
    tobeSign = json.dumps(my_data)
    s = hashlib.sha256()
    s.update(tobeSign.encode('utf-8'))
    sig = sk.sign(bytes.fromhex(s.hexdigest()))
    my_data["sig"] = sig.hex()
    print("verify res", vk.verify(sig, bytes.fromhex(s.hexdigest())))
    '''
    try:
        os.system("bitcoin-cli callcontract " + AIDcontract + " " + str(uid)+ " " + str(vk.to_string().hex())+" >/dev/null 2>&1")
        os.system("bitcoin-cli generate 1 >/dev/null 2>&1")
    except:
        return "run contract error"
    return "AID : "+str(uid)+", create success"


# List all the AID in the database
def listAID():
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        i = 0
        for aid in data:
            print("AID\n---------------------------------------------------------------")
            print("%d \nUUID : %s \npublickey : %s \nsecretkey : %s\n\n---------------------------------------------------------------" % (i, aid['uid'], aid['pk'], aid['sk']) )
            i+=1


# List all the AC in the database        
def listAC():
    with open(dbAC, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        i = 0
        for ac in data:
            print("AC\n---------------------------------------------------------------")
            print("%d \nissuerAID : %s \nholderAID : %s \ntimestamp : %s \nsubject : %s \nsignature : %s \n\n---------------------------------------------------------------" % (i, ac['issuerAID'], ac['holderAID'], ac['timestamp'], ac['subject'], ac['signature']) )
            i+=1


# Delete an AID by specify its index
def delAID(target):
    data = []
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        if(int(target) >= len(data) or int(target) < 0):
            return "out of index"
        aid = data[int(target)]
        ans = input('Are you sure you want del AID : %d ?\n-----------------------------------------\nUUID : %s \npublickey : %s \nsecretkey : %s\n-----------------------------------------\nenter y to delete:'% (int(target), aid['uid'], aid['pk'], aid['sk']))
        if(ans == 'y' or ans == 'Y'):
            del data[int(target)]
            with open(dbAID, 'w', newline='') as jsonfile:
                json.dump(data, jsonfile)
    return "AID deletion successful"


# Delete an AC by specify its index
def delAC(target):
    data = []
    with open(dbAC, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        if(int(target) >= len(data) or int(target) < 0):
            return "out of index"
        ac = data[int(target)]
        ans = input('Are you sure you want to delete the AC : %d ?\n-----------------------------------------\nissuer : %s \nholder : %s \nsubject : %s \ntimestamp : %s \nsignature : %s \n-----------------------------------------\nenter y/Y to delete:'% (int(target), ac['issuerAID'], ac['holderAID'], ac['subject'], ac['timestamp'], ac['signature']))
        if(ans == 'y' or ans == 'Y'):
            del data[int(target)]
            with open(dbAC, 'w', newline='') as jsonfile:
                json.dump(data, jsonfile)
        else:
            return "nothing is deleted"
    return "AC deletion successful"


# Sign a message by an AID's public key,
# parameter indexAID should be the index of the AID using to sign
def signByAID(indexAID, message):
    data = []
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        if(int(indexAID) >= len(data)  or int(indexAID) < 0):
            return "out of index"

    beSign = bytes(str(message), 'utf-8')
    try:
        sk = SigningKey.from_string(bytes.fromhex(data[int(indexAID)]['sk']), curve=SECP256k1)
        vk = sk.verifying_key
        res = sk.sign(beSign)
        assert vk.verify(res,beSign)
    except:
        return "signing failed"
    return str(data[int(indexAID)]['uid']), str(res.hex())


# Login to an website by an specify AID
def aidLogin(indexAID, loginUrl = "http://127.0.0.1:5001/submitAID"):
    data = []
    authenticateAPI = ''
    message = ''
    r = None
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        if(int(indexAID) >= len(data) or int(indexAID) < 0):
            return 'out of index'
    my_data = {"uid":str(data[int(indexAID)]['uid'])}
    try:
        r = requests.get(loginUrl, data = my_data)
        authenticateAPI = json.loads(r.text)["authAPI"]
        message = json.loads(r.text)["message"]
    except:
        return "request " + loginUrl + " failed"
    if(r == None or len(message)!=20):
        return "invalid AID provided"
    try:
        x, res = signByAID(indexAID, message)
    except:
        return "sign failed"
    webbrowser.open("/".join(loginUrl.split("/")[:-1])+'/'+authenticateAPI+"?uid="+my_data['uid']+"&sig="+res)
    return str("uid : "+str(my_data)+ "sign result : "+ res)


# Login to an website by an specify AC
def acLogin(indexAC, loginUrl = "http://127.0.0.1:5001/submitAC"):
    data = []
    AC = []
    holderAID = ""
    authenticateAPI = ''
    message = ''
    with open(dbAC, 'r', newline='') as jsonfile:
        AC = json.load(jsonfile)
        if(int(indexAC) >= len(AC) or int(indexAC) < 0):
            return 'out of index'
    holderUID = AC[int(indexAC)]['holderAID']
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
    for aid in data:
        if (aid['uid']==holderUID):
            holderAID=aid
    my_data = {'AC': json.dumps(AC[int(indexAC)])}
    try:
        r = requests.get(loginUrl, data = my_data)
        authenticateAPI = json.loads(r.text)["authAPI"]
        message = json.loads(r.text)["message"]
    except:
        return "request " + loginUrl + " failed"
    try:
        x, res = signByAID(indexAC, message)
    except:
        return "sign failed"
    webbrowser.open("/".join(loginUrl.split("/")[:-1])+'/'+authenticateAPI+"?aid="+holderUID+"&sig="+res)
    return str("uid : "+ str(holderAID['uid'])+ "sign result : "+ res)


# Request an AC from an issuer by providing an AID, the document for qualification review, and specify the reuqest api provide by issuer
def requestAC(indexAID, doc, requestURL="http://127.0.0.1:5002/"):
    aid = ''
    sig = ''
    data = []
    authenticateAPI = ''
    message = ''
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        aid = data[int(indexAID)]
    if not os.path.exists(doc):
        return "file not found"
    with open(str(doc), 'rb') as f:
        my_data = {'AID':aid['uid']}
        try:
            r = requests.post(requestURL, files={'file': f}, data = my_data)
            authenticateAPI = json.loads(r.text)["authAPI"]
            message = json.loads(r.text)["message"]
        except:
            return str("request " + str(requestURL) + " failed")
        if(len(message)!=20):
            return str("request " + str(requestURL) + " failed")
        x, sig = signByAID(indexAID, str(message))
    try:
        r = requests.post(str(requestURL+'/'+authenticateAPI), data = {'AID':aid['uid'], 'sig':sig})
    except:
        return str("request " + str("/".join(requestURL.split("/")[:-1])+'/'+authenticateAPI) + " failed")
    print(storeAC(r.text))
    s = hashlib.sha256()
    j = r.text.encode('utf-8')
    s.update(j)
    h = s.hexdigest()
    print("fingerprint is ", h)
    return "AC request success"

# Issue an AC that with content to the holderAID using the AID indexed by issuerAIDindex
def issueAC(holderAID, content, issuerAIDindex):
    s = hashlib.sha256()
    with open(dbAID, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
        if(int(issuerAIDindex) >= len(data) or int(issuerAIDindex) < 0):
            return 'out of index'
        my_data = {"issuerAID":str(data[int(issuerAIDindex)]['uid']), "holderAID":holderAID, "timestamp":str(time.time()), "subject":content}
        j = json.dumps(my_data).encode('utf-8')
        try:
            sk = SigningKey.from_string(bytes.fromhex(data[int(issuerAIDindex)]['sk']), curve=SECP256k1)
            s.update(j)
            sig = sk.sign(bytes.fromhex(s.hexdigest()))
            my_data["signature"] = sig.hex()
        except:
            return "signing failed"
        #print("'"+json.dumps(my_data)+"'")
        #print(json.dumps(my_data))
        s = hashlib.sha256()
        j = json.dumps(my_data).encode('utf-8')
        s.update(j)
        h = s.hexdigest()
        #print("the hash : ", str(h))
        try:
            os.system("bitcoin-cli callcontract " + ACcontract + " register " + str(h)+" >/dev/null 2>&1")
            os.system("bitcoin-cli generate 1 >/dev/null 2>&1")
        except:
            return "run contract error"
        #return str("AC generate for issue success\n"+"AC : "+json.dumps(my_data)+"\n hash : "+str(h))
        return json.dumps(my_data)


# Store an AC to local database
def storeAC(AC):    
    theAC = json.loads(str(AC))
    '''
    verify AC
    '''
    data = []
    with open(dbAC, 'r', newline='') as jsonfile:
        data = json.load(jsonfile)
    if(theAC in data):
        return "AC already exist"
    with open(dbAC, 'w', newline='') as jsonfile:
        data.append(theAC)
        json.dump(data, jsonfile)
    return "store AC:\n" + AC + "\nsuccess"


# Rate an AC by giving the AC, a rating 1~5, and the AID index using to rate
def rateAC(ACtobeRated, score, raterAIDindex):
    with open(dbAID, 'r', newline='') as jsonfile:
        if(int(score)>5 or int(score)<1):
            return "invalid rating score"
        s = hashlib.sha256()
        '''
        good to have AC checking here
        '''
        actoberated = json.loads(ACtobeRated)
        acstring = json.dumps(actoberated)
        s.update(acstring.encode('utf-8'))
        h = s.hexdigest()
        data = json.load(jsonfile)
        if(int(raterAIDindex) >= len(data) or int(raterAIDindex) < 0):
            return 'out of index'
        my_data = {"fingerprint":h, "rater":data[int(raterAIDindex)]['uid'], "rating":str(score), "timestamp":str(time.time())}
        j = json.dumps(my_data).encode('utf-8')
        try:
            sk = SigningKey.from_string(bytes.fromhex(data[int(raterAIDindex)]['sk']), curve=SECP256k1)
            s2 = hashlib.sha256()
            s2.update(j)
            sig = sk.sign(bytes.fromhex(s2.hexdigest()))
            my_data["sig"] = sig.hex()
        except:
            return "sign failed"
        #print("'"+json.dumps(my_data)+"'")
        try:
            os.system("bitcoin-cli callcontract " + ACcontract + " rate " + my_data['fingerprint']+ " " +  my_data['rater']+ " " + my_data['rating']+ " " + my_data['timestamp']+ " " + my_data['sig']+ " " + " >/dev/null 2>&1")
            os.system("bitcoin-cli generate 1  >/dev/null 2>&1")
        except:
            return "run contract error"
        return "rate AC success"

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("give me some command")
    else:
        if sys.argv[1] == "help":
            print("==AID related==")
            print('createAID \ndelAID "index"\nlistAID \nloginAID "index" "url" \nsign "index" "message" \n')
            print("==AC related==")
            print('delAC "index"\nissueAC "holderAID" "subject" "AIDindex"\nlistAC \nloginAC "index" "url" \nrateAC "ACtobeRated" "score" "raterAIDindex" \nrequestAC "indexAID" "docName" "requestURL"\nstoreAC "AC" \n')

        elif sys.argv[1] == "createAID":
            print(createAID())
        elif sys.argv[1] == "listAID":
            listAID()
        elif sys.argv[1] == "listAC":
            listAC()
        elif sys.argv[1] == "loginAID":
            if(len(sys.argv)==3):
                print(aidLogin(sys.argv[2]))
            elif(len(sys.argv)==4):
                print(aidLogin(sys.argv[2], sys.argv[3]))
            else:
                print("invalid argc")
        elif sys.argv[1] == "loginAC":
            if(len(sys.argv)==3):
                print(acLogin(sys.argv[2]))
            elif(len(sys.argv)==4):
                print(acLogin(sys.argv[2], sys.argv[3]))
            else:
                print("invalid argc")
        elif sys.argv[1] == "issueAC": # holderAID, content, issuerAIDindex
            if(len(sys.argv)==5):
                print(issueAC(sys.argv[2], sys.argv[3], sys.argv[4]))
            else:
                print("invalid argc")
        elif sys.argv[1] == "storeAC":
            print(storeAC(sys.argv[2]))
        elif sys.argv[1] == "rateAC": # ACtobeRated, score, raterAIDindex
            if(len(sys.argv)==5):
                print(rateAC(sys.argv[2], sys.argv[3], sys.argv[4]))
            else:
                print("invalid argc")
        elif sys.argv[1] == "delAID":
            if(len(sys.argv)==3):
                delAID(sys.argv[2])
            else:
                print("invalid argc")
        elif sys.argv[1] == "delAC":
            if(len(sys.argv)==3):
                delAC(sys.argv[2])
            else:
                print("invalid argc")
        elif sys.argv[1] == "sign": # indexAID, message
            if(len(sys.argv)==4):
                print(signByAID(sys.argv[2], sys.argv[3]))
            else:
                print("invalid argc")
        elif sys.argv[1] == "requestAC": # indexAID, doc, requestURL
            if(len(sys.argv)==4):
                print(requestAC(sys.argv[2], sys.argv[3]))
            elif(len(sys.argv)==5):
                print(requestAC(sys.argv[2], sys.argv[3], sys.argv[4]))
            else:
                print("invalid argc")
        else:
            raise Exception('no command, use python3 aidwallet.py help')
