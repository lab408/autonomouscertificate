#include <ourcontract.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//ratring structure
typedef struct state{
	char fingerprint[100];    //sha256
    char aid[100];          //rater
    char amount[3];           //rating
    char time[100];       //timestamp
    char signature[1000];     //signature
    // ------------------------
    char fingerprintAC[100];
} State;

int contract_main(int argc, char *argv[])
{
    if(argc==7 && strcmp("rate", argv[1])==0) {
        State states[1000];
        int i, size;

        state_read(&size,sizeof(int));
        state_read(states,size*sizeof(State));

        strcpy(states[size].fingerprintAC, "none");

        char fingerprint[100];
        char aid[100];
        char amount[3];
        char time[100];
        char signature[1000];
        strcpy(fingerprint, argv[2]);
        strcpy(aid, argv[3]);
        strcpy(amount, argv[4]);
        strcpy(time, argv[5]);
        strcpy(signature, argv[6]);
        strcpy(states[size].fingerprint, fingerprint);
        strcpy(states[size].aid, aid);
        strcpy(states[size].amount, amount);
        strcpy(states[size].time, time);
        strcpy(states[size].signature, signature);
        size+=1;
        state_write(&size,sizeof(int));
        state_write(states,size*sizeof(State));
        FILE *out;

        char homepath[20];
	    char filepath[50];
	    strcpy (homepath, getenv("HOME"));
	    sprintf(filepath, "%s/.bitcoin/rate.txt", homepath);
        out=fopen(filepath, "w+");
        if(out != NULL) {
            for(i=0;i<size;i++) {
                if(strcmp("none", states[i].fingerprint)!=0)
                    {fprintf(out, "%s:%s:%s:%s:%s\n",states[i].fingerprint, states[i].aid, states[i].amount, states[i].time, states[i].signature);}
            }
        }

    }
    else if(argc==3 && strcmp("register", argv[1])==0) {
        State states[1000];
        int i, size;

        state_read(&size,sizeof(int));
        state_read(states,size*sizeof(State));

        strcpy(states[size].fingerprint, "none");
        strcpy(states[size].signature, "none");
        strcpy(states[size].amount, "none");
        strcpy(states[size].time, "none");

        char fingerprintAC[100];
        strcpy(fingerprintAC, argv[2]);
        strcpy(states[size].fingerprintAC, fingerprintAC);

        size+=1;
        state_write(&size,sizeof(int));
        state_write(states,size*sizeof(State));
        FILE *out;

        char homepath[20];
	    char filepath[50];
	    strcpy (homepath, getenv("HOME"));
	    sprintf(filepath, "%s/.bitcoin/ac.txt", homepath);
        out=fopen(filepath, "w+");
        if(out != NULL) {
            for(i=0;i<size;i++) {
                if(strcmp("none", states[i].fingerprintAC)!=0)
                    {fprintf(out, "%s\n",states[i].fingerprintAC);}
            }
        }
    }
    else return -1; // no-op

    return 0;
}

