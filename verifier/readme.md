# forum.py
---
**Home Page**
----
  返回首頁.

* **URL**
  /
  
* **Method:**
  `GET`
  
---
**發文**
----
  使用者透過提供新聞張的標題跟內來發布新文章
  新文章以及發布者AID, AC等資訊一同紀錄在posts.json的本地資料庫
 

* **URL**
  /post
  
* **Method:**
  `POST, GET`

* **Data Params**
`title=[string]`
`content=[string]  ` 

  
---
**登錄頁面**
---
  返回一個登錄頁面 

* **URL**
  /login
  
* **Method:**
  `GET`

  
---
**登出**
----
  將使用者的所有session刪除做登出, 並重新載入首頁

* **URL**
  /logout
  
* **Method:**
  `POST, GET`

*  **URL Params**

* **Data Params**
`AID=[uuidv4]`
`sig=[string]  #用來幫助AID身份認證的簽章` 

  
---
**AID登錄**
----
  接收使用者提供的AID,並反還一串長度20的亂碼給使用者進行簽章以通過後續的AID認證
  同時在本地資料庫AIDsubmitters.json中紀錄此AID以此AID在認證時需要簽章的亂碼

* **URL**
  /submitAID
  
* **Method:**
  `POST, GET`

* **Data Params**
`uid=[uuidv4]`
  
---
**AID認證**
----
  根據使用者提交的AID跟簽章來進行身份認證
  驗證流程如下：
  1.先去AIDsubmitters.json找到該AID必須要簽章的亂碼字串為何
  2.去智能合約 "~/.bitcoin/aid.txt"找到該AID之對應公開鑰匙為何
  3.透過使用者提供的簽章, 以及前兩個步驟分別找到的簽章前的亂碼字串跟公鑰來進行驗章的步驟
  4.一旦驗章成功AID認證就是成功了

* **URL**
  /authenticateAID
  
* **Method:**
  `POST, GET`

*  **URL Params**

* **Data Params**
`uid=[uuidv4]`
`sig=[string]  #用來幫助AID身份認證的簽章` 

  
---
**AC登錄**
----
  接收使用者提供的AC,在透過verificationAC模組verify該AC後,反還一串長度20的亂碼給使用者進行簽章以通過後續的AC認證
  AC認證其實就是對他上面的holderAID進行AID認證
  同時在本地資料庫ACsubmitters.json中紀錄此AC以及此AC在認證時需要簽章的亂碼

* **URL**
  /submitAC
  
* **Method:**
  `POST, GET`

*  **URL Params**

* **Data Params**
`AC=[string] # 這邊必須遵守AC的json字串格式來提交,一個範例如下:
Note:Key的名稱必須要一樣,這是默認的共識

{"issuerAID": "39815f49-a9e8-4852-aac9-11b6a4fa694a", "holderAID": "e8d13170-0610-46d1-b177-cf29bf209748", "timestamp": "1630647403.1676748", "subject": "lab408", "signature": "96337927c3e86b4ef14b23496f93979795943e05c89355b266879ad196d387aec0e95b1618f84133a362882f5f2ebc6e82f834acdb0958c79d05e2df8213e5a7"}` 


  
---
**AC認證**
----
  根據使用者提交的holderAID跟簽章來進行身份認證
  Note: holderAID及是某個AC所榜定的AID,意味著該AC是由該holder所持有
  驗證流程如下：
  1.先去ACsubmitters.json找到該holderAID必須要簽章的亂碼字串為何
  2.去智能合約 "~/.bitcoin/aid.txt"找到該AID之對應公開鑰匙為何
  3.透過使用者提供的簽章, 以及前兩個步驟分別找到的簽章前的亂碼字串跟公鑰來進行驗章的步驟
  4.一旦驗章成功AC認證就是成功了

* **URL**
  /authenticateAC
  
* **Method:**
  `POST, GET`

*  **URL Params**

* **Data Params**
`aid=[uuidv4] # holder AID之uid` 
`sig=[string]  #用來幫助AC身份認證的簽章` 

  
---

# verificationAC module
 這個模組只有一個function :verifyAC(AC)
 這個function主要只做兩件事
 1.驗證AC上的簽章,確保該AC的確是由紀錄在上面的issuerAID所發布
 2.查詢該AC的註冊紀錄
 一旦這兩項工作完成,我們才能確保這是一個合法的AC